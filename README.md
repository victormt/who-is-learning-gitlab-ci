# Who is learning GitLab CI?

## Getting started

- Watch the [GitLab CI course on freecodecamp](https://youtube.com)
- Check the [course notes](https://gitlab.com/...)
